class Coche extends Vehiculo {

    public nRuedas: number;
    
    constructor(plazas: number) {
        super(plazas, tiposVehiculo.terrestre);
    }

    protected desplazar(): void {
        this.giraRuedas();
    }

    private giraRuedas() {

    }

}